package com.github.jschmidt10.delete.check;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.accumulo.core.client.BatchWriter;
import org.apache.accumulo.core.client.BatchWriterConfig;
import org.apache.accumulo.core.client.Connector;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Mutation;
import org.apache.accumulo.core.data.Value;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.serializer.KryoSerializer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import scala.Tuple2;

public class DeleteCheckJobTest {

  private String baseTable = "baseTable";

  private MockConnectorFactory connectorFactory;
  private JavaSparkContext sc;
  private DeleteCheckJob deleteCheckJob;

  @Before
  public void setup() {
    SparkConf conf =
        new SparkConf()
            .setMaster("local[2]")
            .setAppName("unittest")
            .set("spark.ui.enabled", "false")
            .set("spark.serializer", KryoSerializer.class.getName());

    conf.registerKryoClasses(new Class<?>[] {Key.class, Value.class, KV.class});

    sc = new JavaSparkContext(conf);
    connectorFactory = new MockConnectorFactory();

    AccumuloProperties props = new AccumuloProperties("root", "", "mock-instance", "localhost:2181");

    BuildCheckConf buildCheckConf = new BuildCheckConf.Builder().build();
    RunCheckConf runCheckConf = new RunCheckConf.Builder().baseTable(baseTable).connectorFactory(connectorFactory).build();

    deleteCheckJob = new DeleteCheckJob(sc, props, buildCheckConf, runCheckConf);
  }

  @After
  public void tearDown() {
    sc.close();
  }

  @Test
  public void shouldBuildChecksFromKeepsAndDeletes() {
    KV row1Keep = new KVBuilder().dataset(Dataset.KEEP).row("row1").timestamp(0L).build();
    KV row1Delete = new KVBuilder().dataset(Dataset.DELETE).row("row1").timestamp(1L).build();
    KV row2Keep = new KVBuilder().dataset(Dataset.KEEP).row("row2").timestamp(1L).build();
    KV row2Delete = new KVBuilder().dataset(Dataset.DELETE).row("row2").timestamp(0L).build();
    KV row3Keep = new KVBuilder().dataset(Dataset.KEEP).row("row3").build();
    KV row4Delete = new KVBuilder().dataset(Dataset.DELETE).row("row4").build();

    List<KV> keepKVs = Arrays.asList(row1Keep, row2Keep, row3Keep);
    List<KV> deleteKVs = Arrays.asList(row1Delete, row2Delete, row4Delete);

    JavaPairRDD<Key, Value> keepRdd = rdd(sc, keepKVs);
    JavaPairRDD<Key, Value> deleteRdd = rdd(sc, deleteKVs);

    List<DeleteCheck> actualDeleteChecks = deleteCheckJob.buildChecks(keepRdd, deleteRdd).collect();

    assertThat(actualDeleteChecks.size(), is(4));
    assertThat(actualDeleteChecks.contains(new DeleteCheck(row1Keep, row1Delete)), is(true));
    assertThat(actualDeleteChecks.contains(new DeleteCheck(row2Keep, row2Delete)), is(true));
    assertThat(actualDeleteChecks.contains(new DeleteCheck(row3Keep, null)), is(true));
    assertThat(actualDeleteChecks.contains(new DeleteCheck(null, row4Delete)), is(true));
  }

  @Test
  public void shouldRunChecks() throws Exception {
    Connector connector = connectorFactory.newConnector();

    // row1 is kept successfully
    // row2 is deleted successfully
    // row3 is incorrectly deleted
    Key row1 = new Key("row1", "", "");
    Key row2 = new Key("row2", "", "");
    Key row3 = new Key("row3", "", "");

    insertAccumulo(connector, baseTable, row1);

    DeleteCheck checkRow1 = new DeleteCheck(new KV(row1, null), null);
    DeleteCheck checkRow2 = new DeleteCheck(null, new KV(row2, null));
    DeleteCheck checkRow3 = new DeleteCheck(new KV(row3, null), null);

    JavaRDD<DeleteCheck> checkRdd = sc.parallelize(Arrays.asList(checkRow1, checkRow2, checkRow3));

    List<CheckResult> checkResults = deleteCheckJob.runChecks(checkRdd).collect();

    assertThat(checkResults.size(), is(1));
    assertThat(checkResults.get(0), is(CheckResult.shouldHaveBeenKept(row3)));
  }

  private void insertAccumulo(Connector connector, String table, Key key) throws Exception {
    if (!connector.tableOperations().exists(table)) {
      connector.tableOperations().create(table);
    }

    try (BatchWriter bw = connector.createBatchWriter(table, new BatchWriterConfig())) {
      Mutation mutation = new Mutation(key.getRow());
      mutation.put(key.getColumnFamily(), key.getColumnQualifier(), new Value());

      bw.addMutation(mutation);
    }
  }

  private JavaPairRDD<Key, Value> rdd(JavaSparkContext sc, List<KV> kvs) {
    List<Tuple2<Key, Value>> pairList = kvs.stream().map(this::tuple).collect(Collectors.toList());

    return sc.parallelizePairs(pairList);
  }

  private Tuple2<Key, Value> tuple(Map.Entry<Key, Value> entry) {
    return new Tuple2<>(entry.getKey(), entry.getValue());
  }
}
