package com.github.jschmidt10.delete.check;

import org.apache.accumulo.core.client.AccumuloException;
import org.apache.accumulo.core.client.AccumuloSecurityException;
import org.apache.accumulo.core.client.Connector;
import org.apache.accumulo.core.client.mock.MockInstance;
import org.apache.accumulo.core.client.security.tokens.PasswordToken;

public class MockConnectorFactory implements ConnectorFactory {

  // Use static so that it's shared across calls to imitate an external resource
  private static Connector connector;

  static {
    try {
      connector = new MockInstance().getConnector("root", new PasswordToken(new byte[0]));
    } catch (AccumuloException | AccumuloSecurityException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public Connector newConnector() throws AccumuloException, AccumuloSecurityException {
    return connector;
  }
}
