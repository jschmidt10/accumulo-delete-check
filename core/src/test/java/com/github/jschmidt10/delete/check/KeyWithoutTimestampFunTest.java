package com.github.jschmidt10.delete.check;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.apache.accumulo.core.data.Key;
import org.junit.Test;

public class KeyWithoutTimestampFunTest {

  @Test
  public void shouldCreateKeyWithoutTimestamp() {
    String row = "row1";
    long timestamp = 100L;

    KV original = new KVBuilder().row(row).timestamp(timestamp).build();
    KV expected = new KVBuilder().row(row).timestamp(0L).build();

    Key actual = new KeyWithoutTimestampFun().call(original);

    assertThat(actual, is(expected.getKey()));
  }
}
