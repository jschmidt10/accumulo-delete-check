package com.github.jschmidt10.delete.check;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Value;
import org.junit.Test;
import scala.Tuple2;

public class FilterIgnoredKeysFunTest {

  private String keepKey = "keep";
  private String ignoredKey = "ignore";

  @Test
  public void shouldFilterIgnoredKeys() {
    Tuple2<Key, Value> keep = new KVBuilder().row(this.keepKey).buildTuple();
    Tuple2<Key, Value> ignored = new KVBuilder().row(this.ignoredKey).buildTuple();

    FilterIgnoredKeysFun filter = new FilterIgnoredKeysFun(".*" + this.ignoredKey + ".*");

    assertThat(filter.call(keep), is(true));
    assertThat(filter.call(ignored), is(false));
  }
}
