package com.github.jschmidt10.delete.check;

import java.nio.charset.StandardCharsets;
import java.util.Objects;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Value;
import scala.Tuple2;

/** Test utility for building Accumulo entries. */
public final class KVBuilder {

  private Dataset dataset = null;
  private String row = "";
  private String fam = "";
  private String qual = "";
  private long timestamp = 0L;
  private String val = "";

  /**
   * Set the dataset.
   *
   * @param dataset The dataset
   * @return this
   */
  public KVBuilder dataset(Dataset dataset) {
    this.dataset = Objects.requireNonNull(dataset);
    return this;
  }

  /**
   * Set the key row.
   *
   * @param row The key row
   * @return this
   */
  public KVBuilder row(String row) {
    this.row = Objects.requireNonNull(row);
    return this;
  }

  /**
   * Set the key column family.
   *
   * @param fam The key column family
   * @return this
   */
  public KVBuilder fam(String fam) {
    this.fam = Objects.requireNonNull(fam);
    return this;
  }

  /**
   * Set the key column qualifier.
   *
   * @param qual The key column qualifier
   * @return this
   */
  public KVBuilder qual(String qual) {
    this.qual = Objects.requireNonNull(qual);
    return this;
  }

  /**
   * Set the key timestamp.
   *
   * @param timestamp The key timestamp
   * @return this
   */
  public KVBuilder timestamp(long timestamp) {
    this.timestamp = timestamp;
    return this;
  }

  /**
   * Set the value.
   *
   * @param val The value
   * @return this
   */
  public KVBuilder val(String val) {
    this.val = Objects.requireNonNull(val);
    return this;
  }

  /**
   * Builds the key/value pair.
   *
   * @return A new key/value pair.
   */
  public KV build() {
    Key key = new Key(row, fam, qual, timestamp);
    Value value = new Value(val.getBytes(StandardCharsets.UTF_8));
    return new KV(dataset, key, value);
  }

  /**
   * Builds the key/value pair as a Scala tuple.
   *
   * @return A new key/value pair as a tuple.
   */
  public Tuple2<Key, Value> buildTuple() {
    KV kv = build();
    return new Tuple2<>(kv.getKey(), kv.getValue());
  }
}
