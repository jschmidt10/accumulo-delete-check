package com.github.jschmidt10.delete.check;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Test;

public class BasicTableStrategyTest {

  private String row = "row1";
  private long timestamp = 0;

  private BasicTableStrategy tableStrategy = new BasicTableStrategy();

  @Test
  public void shouldKeepIfOnlyKeep() {
    KV scanned = new KVBuilder().row(row).build();
    KV keep = new KVBuilder().row(row).build();

    assertThat(tableStrategy.validate(scanned, keep, null), is(true));
  }

  @Test
  public void shouldKeepIfKeepNewerThanDelete() {
    KV scanned = new KVBuilder().row(row).build();
    KV keep = new KVBuilder().row(row).timestamp(timestamp + 1).build();
    KV delete = new KVBuilder().row(row).timestamp(timestamp).build();

    assertThat(tableStrategy.validate(scanned, keep, delete), is(true));
  }

  @Test
  public void shouldFailIfExpectKeepButScannedDoesNotExist() {
    KV keep = new KVBuilder().row(row).build();

    assertThat(tableStrategy.validate(null, keep, null), is(false));
  }

  @Test
  public void shouldDeleteIfOnlyDelete() {
    KV delete = new KVBuilder().row(row).build();

    assertThat(tableStrategy.validate(null, null, delete), is(true));
  }

  @Test
  public void shouldDeleteIfDeleteNewerThanKeep() {
    KV keep = new KVBuilder().row(row).timestamp(timestamp).build();
    KV delete = new KVBuilder().row(row).timestamp(timestamp + 1).build();

    assertThat(tableStrategy.validate(null, keep, delete), is(true));
  }

  @Test
  public void shouldFailIfExpectDeleteButScannedExists() {
    KV scanned = new KVBuilder().row(row).build();
    KV delete = new KVBuilder().row(row).build();

    assertThat(tableStrategy.validate(scanned, null, delete), is(false));
  }

  @Test(expected = IllegalArgumentException.class)
  public void shouldErrorIfBothDeleteAndKeepAreNull() {
    tableStrategy.validate(null, null, null);
  }

  @Test(expected = IllegalArgumentException.class)
  public void shouldErrorWhenTimestampsAreTied() {
    KV keep = new KVBuilder().row(row).timestamp(timestamp).build();
    KV delete = new KVBuilder().row(row).timestamp(timestamp).build();

    tableStrategy.validate(null, keep, delete);
  }
}
