package com.github.jschmidt10.delete.check;

import java.nio.charset.StandardCharsets;
import java.util.Objects;
import org.apache.accumulo.core.client.AccumuloException;
import org.apache.accumulo.core.client.AccumuloSecurityException;
import org.apache.accumulo.core.client.Connector;
import org.apache.accumulo.core.client.ZooKeeperInstance;
import org.apache.accumulo.core.client.security.tokens.PasswordToken;

/** Creates connectors to an Accumulo instance. */
public class AccumuloConnectorFactory implements ConnectorFactory {

  private AccumuloProperties props;

  public AccumuloConnectorFactory(AccumuloProperties props) {
    this.props = Objects.requireNonNull(props);
  }

  @Override
  public Connector newConnector() throws AccumuloException, AccumuloSecurityException {
    ZooKeeperInstance zki = new ZooKeeperInstance(props.instanceName, props.zookeepers);
    PasswordToken passwordToken = new PasswordToken(props.password.getBytes(StandardCharsets.UTF_8));

    return zki.getConnector(props.username, passwordToken);
  }
}
