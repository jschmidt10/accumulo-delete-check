package com.github.jschmidt10.delete.check;

import java.util.Map;
import java.util.Objects;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Range;
import org.apache.accumulo.core.data.Value;

/** All the information needed to perform an Accumulo deletion check. */
public class DeleteCheck {

  private final Range range;
  private final Map.Entry<Key, Value> keepEntry;
  private final Map.Entry<Key, Value> deleteEntry;

  public DeleteCheck(Map.Entry<Key, Value> keepEntry, Map.Entry<Key, Value> deleteEntry) {
    if (keepEntry == null && deleteEntry == null) {
      throw new IllegalArgumentException("Both keepEntry and deleteEntry cannot be null.");
    }

    this.keepEntry = keepEntry;
    this.deleteEntry = deleteEntry;
    this.range = getRange(keepEntry, deleteEntry);
  }

  private Range getRange(Map.Entry<Key, Value> keepEntry, Map.Entry<Key, Value> deleteEntry) {
    if (keepEntry == null) {
      return toRange(deleteEntry);
    } else {
      return toRange(keepEntry);
    }
  }

  /**
   * Convert an entry into a Range for just the key (minus the timestamp).
   *
   * @param entry An entry
   * @return A Range
   */
  public static Range toRange(Map.Entry<Key, Value> entry) {
    Key key = entry.getKey();
    return Range.exact(key.getRow(), key.getColumnFamily(), key.getColumnQualifier(), key.getColumnVisibility());
  }

  public Range getRange() {
    return range;
  }

  public Map.Entry<Key, Value> getKeepEntry() {
    return keepEntry;
  }

  public Map.Entry<Key, Value> getDeleteEntry() {
    return deleteEntry;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    DeleteCheck deleteCheck = (DeleteCheck) o;
    return range.equals(deleteCheck.range)
        && Objects.equals(keepEntry, deleteCheck.keepEntry)
        && Objects.equals(deleteEntry, deleteCheck.deleteEntry);
  }

  @Override
  public int hashCode() {
    return Objects.hash(range, keepEntry, deleteEntry);
  }

  @Override
  public String toString() {
    return "Check{" + "range=" + range + ", keepEntry=" + keepEntry + ", deleteEntry=" + deleteEntry + '}';
  }
}
