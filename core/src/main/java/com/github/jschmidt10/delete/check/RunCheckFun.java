package com.github.jschmidt10.delete.check;

import java.io.Serializable;
import java.util.*;
import org.apache.accumulo.core.client.AccumuloException;
import org.apache.accumulo.core.client.AccumuloSecurityException;
import org.apache.accumulo.core.client.BatchScanner;
import org.apache.accumulo.core.client.Connector;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Range;
import org.apache.accumulo.core.data.Value;
import org.apache.accumulo.core.security.Authorizations;
import org.apache.spark.api.java.function.FlatMapFunction;

/**
 * A Spark Function for running the checks against the Accumulo base table.
 *
 * <p>This function will only return FAILED check results for efficiency.
 */
public class RunCheckFun implements FlatMapFunction<Iterator<DeleteCheck>, CheckResult>, Serializable {

  private final RunCheckConf conf;

  public RunCheckFun(RunCheckConf conf) {
    this.conf = conf;
  }

  @Override
  public Iterator<CheckResult> call(Iterator<DeleteCheck> iter) throws Exception {
    List<CheckResult> failedChecks = new ArrayList<>(conf.batchSize);

    Connector connector = conf.connectorFactory.newConnector();
    try (BatchScanner scanner = connector.createBatchScanner(conf.baseTable, getAuths(connector), conf.scanThreads)) {
      while (iter.hasNext()) {
        Map<Range, DeleteCheck> batch = getNextBatch(iter);
        failedChecks.addAll(getFailedFoundKeys(scanner, batch));
        failedChecks.addAll(getFailedMissingKeys(batch));
      }
    }

    return failedChecks.iterator();
  }

  private List<CheckResult> getFailedFoundKeys(BatchScanner scanner, Map<Range, DeleteCheck> batch) {
    List<CheckResult> failedChecks = new ArrayList<>(conf.batchSize);

    scanner.setRanges(batch.keySet());

    for (Map.Entry<Key, Value> entry : scanner) {
      Range range = DeleteCheck.toRange(entry);
      DeleteCheck deleteCheck = batch.remove(range);

      if (deleteCheck == null) {
        throw new IllegalStateException("Found a key that doesn't match any of the ranges: " + entry.getKey());
      }

      boolean isSuccess = conf.tableStrategy.validate(entry, deleteCheck.getKeepEntry(), deleteCheck.getDeleteEntry());

      if (!isSuccess) {
        failedChecks.add(CheckResult.shouldHaveBeenDeleted(entry.getKey()));
      }
    }

    return failedChecks;
  }

  private List<CheckResult> getFailedMissingKeys(Map<Range, DeleteCheck> batch) {
    List<CheckResult> failedChecks = new ArrayList<>(conf.batchSize);

    for (Map.Entry<Range, DeleteCheck> missedRanges : batch.entrySet()) {
      DeleteCheck deleteCheck = missedRanges.getValue();

      boolean isSuccess = conf.tableStrategy.validate(null, deleteCheck.getKeepEntry(), deleteCheck.getDeleteEntry());

      if (!isSuccess) {
        failedChecks.add(CheckResult.shouldHaveBeenKept(deleteCheck.getKeepEntry().getKey()));
      }
    }

    return failedChecks;
  }

  private Authorizations getAuths(Connector connector) throws AccumuloSecurityException, AccumuloException {
    return connector.securityOperations().getUserAuthorizations(connector.whoami());
  }

  private Map<Range, DeleteCheck> getNextBatch(Iterator<DeleteCheck> iter) {
    Map<Range, DeleteCheck> batch = new HashMap<>(conf.batchSize * 2);
    while (batch.size() < conf.batchSize && iter.hasNext()) {
      DeleteCheck deleteCheck = iter.next();
      batch.put(deleteCheck.getRange(), deleteCheck);
    }
    return batch;
  }
}
