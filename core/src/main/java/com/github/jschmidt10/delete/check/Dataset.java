package com.github.jschmidt10.delete.check;

/** The types of datasets used in the check tool. */
enum Dataset {
  KEEP,
  DELETE
}
