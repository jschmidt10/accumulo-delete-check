package com.github.jschmidt10.delete.check;

import java.util.Map;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Value;

/**
 * Validates basic Accumulo table behavior.
 *
 * <p>If the keep entry is newer than the delete key, or the delete key is not present, we expect a result. Otherwise, we expect no result.
 */
public class BasicTableStrategy implements TableStrategy {

  @Override
  public boolean validate(Map.Entry<Key, Value> scanned, Map.Entry<Key, Value> keepEntry, Map.Entry<Key, Value> deleteEntry) {
    if (keepEntry == null && deleteEntry == null) {
      throw new IllegalArgumentException("Both keep and delete entries cannot be null");
    } else if (keepEntry != null && (deleteEntry == null || ts(keepEntry) > ts(deleteEntry))) {
      return scanned != null;
    } else if (keepEntry == null || ts(keepEntry) < ts(deleteEntry)) {
      return scanned == null;
    } else {
      // ts(keepEntry) == ts(deleteEntry)
      throw new IllegalArgumentException("The behavior of a keep entry and a delete entry with the same timestamp is undefined.");
    }
  }

  private long ts(Map.Entry<Key, Value> entry) {
    return entry.getKey().getTimestamp();
  }
}
