package com.github.jschmidt10.delete.check;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import org.apache.accumulo.core.client.security.tokens.PasswordToken;

/** Accumulo connection properties. */
public final class AccumuloProperties implements Serializable {

  public final String username;
  public final String password;
  public final String instanceName;
  public final String zookeepers;

  /**
   * Constructs a new {@link AccumuloProperties}
   *
   * @param username Username
   * @param password Password
   * @param instanceName Instance name
   * @param zookeepers Zookeepers
   * @throws IllegalArgumentException if any of the parameters are blank
   */
  public AccumuloProperties(String username, String password, String instanceName, String zookeepers) {
    this.username = Args.requireNotBlank(username, "username");
    this.password = Objects.requireNonNull(password, "password must not be null");
    this.instanceName = Args.requireNotBlank(instanceName, "instanceName");
    this.zookeepers = Args.requireNotBlank(zookeepers, "zookeepers");
  }

  /**
   * Creates a new PasswordToken
   *
   * @return PasswordToken
   */
  public PasswordToken getPasswordToken() {
    return new PasswordToken(password.getBytes(StandardCharsets.UTF_8));
  }
}
