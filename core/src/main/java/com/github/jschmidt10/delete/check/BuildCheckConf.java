package com.github.jschmidt10.delete.check;

import java.io.Serializable;

/** The configurations needed by the "build checks" portion of {@link DeleteCheckJob}. */
public final class BuildCheckConf implements Serializable {
  public final String ignoredKeyRegex;
  public final Integer desiredPartitions;

  public BuildCheckConf(String ignoredKeyRegex, Integer desiredPartitions) {
    this.ignoredKeyRegex = ignoredKeyRegex;
    this.desiredPartitions = desiredPartitions;
  }

  /** Builder for {@link BuildCheckConf} */
  public static class Builder {
    private String ignoredKeyRegex;
    private Integer desiredPartitions;

    public Builder ignoredKeyRegex(String ignoredKeyRegex) {
      this.ignoredKeyRegex = ignoredKeyRegex;
      return this;
    }

    public Builder desiredPartitions(Integer desiredPartitions) {
      this.desiredPartitions = desiredPartitions;
      return this;
    }

    public BuildCheckConf build() {
      return new BuildCheckConf(ignoredKeyRegex, desiredPartitions);
    }
  }
}
