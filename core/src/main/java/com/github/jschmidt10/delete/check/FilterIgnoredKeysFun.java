package com.github.jschmidt10.delete.check;

import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Value;
import org.apache.spark.api.java.function.Function;
import scala.Tuple2;

/** Spark function which filters ignored keys if given (if no ignoredKeyRegex is given, all keys are returned). */
public class FilterIgnoredKeysFun implements Function<Tuple2<Key, Value>, Boolean> {

  private final String ignoredKeyRegex;

  public FilterIgnoredKeysFun(String ignoredKeyRegex) {
    this.ignoredKeyRegex = ignoredKeyRegex;
  }

  @Override
  public Boolean call(Tuple2<Key, Value> tuple) {
    if (ignoredKeyRegex == null) {
      return true;
    } else {
      return !tuple._1.toStringNoTruncate().matches(ignoredKeyRegex);
    }
  }
}
