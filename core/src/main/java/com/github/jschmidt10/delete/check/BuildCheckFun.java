package com.github.jschmidt10.delete.check;

import org.apache.accumulo.core.data.Key;
import org.apache.spark.api.java.function.Function;
import scala.Tuple2;

/** Given the grouped entries, construct the DeleteCheck to perform. */
class BuildCheckFun implements Function<Tuple2<Key, Iterable<KV>>, DeleteCheck> {
  @Override
  public DeleteCheck call(Tuple2<Key, Iterable<KV>> tuple) {
    KV keepKV = null;
    KV deleteKV = null;

    for (KV kv : tuple._2) {
      switch (kv.getDataset()) {
        case KEEP:
          keepKV = kv;
          break;
        case DELETE:
          deleteKV = kv;
          break;
      }
    }

    return new DeleteCheck(keepKV, deleteKV);
  }
}
