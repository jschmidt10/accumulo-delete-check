package com.github.jschmidt10.delete.check;

import java.util.Map;
import java.util.Objects;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Value;

/**
 * A wrapper around a key/value pair for more concise references.
 *
 * <p>This wrapper adds an optional Dataset tag.
 */
public class KV implements Map.Entry<Key, Value> {

  private Dataset dataset;
  private Key key;
  private Value value;

  public KV() {
    this(null, null, null);
  }

  public KV(Key key, Value value) {
    this(null, key, value);
  }

  public KV(Dataset dataset, Key key, Value value) {
    this.dataset = dataset;
    this.key = key;
    this.value = value;
  }

  public Dataset getDataset() {
    return dataset;
  }

  public void setDataset(Dataset dataset) {
    this.dataset = dataset;
  }

  @Override
  public Key getKey() {
    return key;
  }

  @Override
  public Value getValue() {
    return value;
  }

  @Override
  public Value setValue(Value value) {
    Value prev = this.value;
    this.value = value;
    return prev;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    KV kv = (KV) o;
    return dataset == kv.dataset && Objects.equals(key, kv.key) && Objects.equals(value, kv.value);
  }

  @Override
  public int hashCode() {
    return Objects.hash(dataset, key, value);
  }

  @Override
  public String toString() {
    return "KV{" + "dataset=" + dataset + ", key=" + key + ", value=" + value + '}';
  }
}
