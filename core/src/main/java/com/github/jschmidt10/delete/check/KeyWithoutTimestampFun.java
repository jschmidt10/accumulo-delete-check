package com.github.jschmidt10.delete.check;

import java.io.Serializable;
import org.apache.accumulo.core.data.Key;
import org.apache.spark.api.java.function.Function;

/** A function to use with Spark's keyBy() function which ignores the timestamp of an Accumulo Key. */
final class KeyWithoutTimestampFun implements Function<KV, Key>, Serializable {
  @Override
  public Key call(KV kv) {
    Key withoutTimestamp = new Key(kv.getKey());
    withoutTimestamp.setTimestamp(0L);
    return withoutTimestamp;
  }
}
