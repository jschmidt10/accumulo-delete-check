package com.github.jschmidt10.delete.check;

import java.io.Serializable;
import java.util.Map;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Value;

/** Evaluates if a key/value pair was deleted or kept correctly. */
public interface TableStrategy extends Serializable {

  /**
   * Validates the scanned entry against the corresponding keep and delete entries.
   *
   * <p>Any of these entries can be null. A null in the scanned parameter means that the scan found no results.
   *
   * @param scanned The scanned entry from the base table or null if no entry was found.
   * @param keep The entry from the keep dataset or null if there was no entry in the keep dataset.
   * @param delete The entry from the delete dataset or null if there was no entry in the delete dataset.
   * @return True if the scanned entry was correct, false, otherwise.
   */
  boolean validate(Map.Entry<Key, Value> scanned, Map.Entry<Key, Value> keep, Map.Entry<Key, Value> delete);
}
