package com.github.jschmidt10.delete.check;

import java.nio.file.Files;
import java.nio.file.Path;

/** Argument validation helpers. */
public final class Args {

  private Args() {}

  /**
   * Requires that a file path is writable (or doesn't yet exist).
   *
   * @param path The file path
   * @param name The name of the variable (used by the error message)
   * @return The file if it is writable
   * @throws IllegalArgumentException if path is not writable
   */
  public static Path requireFileWritable(Path path, String name) {
    if (Files.exists(path) && !Files.isWritable(path)) {
      throw new IllegalArgumentException(name + " must be writable");
    }
    return path;
  }

  /**
   * Requires that a String is not blank.
   *
   * @param string The String
   * @param name The name of the variable (used by the error message)
   * @return The String if valid
   * @throws IllegalArgumentException if string is null or blank
   */
  public static String requireNotBlank(String string, String name) {
    if (string == null || string.isBlank()) {
      throw new IllegalArgumentException(name + " must not be blank");
    }
    return string;
  }

  /**
   * Requires that a number be greater than zero.
   *
   * @param number The number
   * @param name The name of the variable (used by the error message)
   * @return The number if valid
   * @throws IllegalArgumentException if number <= 0
   */
  public static int requireGreaterThanZero(int number, String name) {
    if (number <= 0) {
      throw new IllegalArgumentException(name + " must be greater than zero but was " + number);
    }
    return number;
  }
}
