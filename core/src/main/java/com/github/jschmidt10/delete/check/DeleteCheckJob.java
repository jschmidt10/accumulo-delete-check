package com.github.jschmidt10.delete.check;

import java.io.IOException;
import java.util.Collection;
import java.util.Objects;
import org.apache.accumulo.core.client.AccumuloException;
import org.apache.accumulo.core.client.AccumuloSecurityException;
import org.apache.accumulo.core.client.ClientConfiguration;
import org.apache.accumulo.core.client.mapreduce.AccumuloInputFormat;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Value;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.mapreduce.Job;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

/** Given the keep and delete tables, build the {@link DeleteCheck} objects. */
public class DeleteCheckJob {

  private final JavaSparkContext sc;
  private final AccumuloProperties props;
  private final BuildCheckConf buildCheckConf;
  private final RunCheckConf runCheckConf;

  /**
   * Creates a new DeleteCheckJob
   *
   * @param sc The Spark Context
   * @param accumuloProperties The Accumulo connection properties
   */
  public DeleteCheckJob(JavaSparkContext sc, AccumuloProperties accumuloProperties, BuildCheckConf buildCheckConf, RunCheckConf runCheckConf) {
    this.sc = Objects.requireNonNull(sc);
    this.props = Objects.requireNonNull(accumuloProperties);
    this.buildCheckConf = Objects.requireNonNull(buildCheckConf);
    this.runCheckConf = Objects.requireNonNull(runCheckConf);
  }

  /**
   * Check the base table against the keep and delete tables.
   *
   * @param keepTable The table containing the keys to keep
   * @param deleteTable The table containing the keys to delete
   * @return All failed checks
   */
  public Collection<CheckResult> run(String keepTable, String deleteTable) throws IOException, AccumuloSecurityException, AccumuloException {
    JavaPairRDD<Key, Value> keepRdd = repartition(filterKeys(tableRdd(keepTable)));
    JavaPairRDD<Key, Value> deleteRdd = repartition(filterKeys(tableRdd(deleteTable)));

    return runChecks(buildChecks(keepRdd, deleteRdd)).collect();
  }

  private JavaPairRDD<Key, Value> repartition(JavaPairRDD<Key, Value> tableRdd) {
    if (buildCheckConf.desiredPartitions != null) {
      return tableRdd.repartition(buildCheckConf.desiredPartitions);
    } else {
      return tableRdd;
    }
  }

  private JavaPairRDD<Key, Value> filterKeys(JavaPairRDD<Key, Value> tableRdd) {
    return tableRdd.filter(new FilterIgnoredKeysFun(buildCheckConf.ignoredKeyRegex));
  }

  private JavaPairRDD<Key, Value> tableRdd(String table) throws AccumuloSecurityException, AccumuloException, IOException {
    Configuration conf = configureAccumuloInput(table);
    return sc.newAPIHadoopRDD(conf, AccumuloInputFormat.class, Key.class, Value.class);
  }

  private Configuration configureAccumuloInput(String table) throws IOException, AccumuloSecurityException, AccumuloException {
    Job job = Job.getInstance();

    AccumuloInputFormat.setZooKeeperInstance(job, ClientConfiguration.create().withInstance(props.instanceName).withZkHosts(props.zookeepers));
    AccumuloInputFormat.setConnectorInfo(job, props.username, props.getPasswordToken());
    AccumuloInputFormat.setInputTableName(job, table);
    AccumuloInputFormat.setScanAuthorizations(job, ConnectorFactory.getAllAuths(new AccumuloConnectorFactory(props)));

    return job.getConfiguration();
  }

  /**
   * Given the keep and delete rdds, build an rdd contain the {@link DeleteCheck}s to be performed.
   *
   * @param keepRdd An rdd of key/values from the keep table.
   * @param deleteRdd An rdd of key/values from the delete table.
   * @return An rdd containing the delete checks to perform.
   */
  JavaRDD<DeleteCheck> buildChecks(JavaPairRDD<Key, Value> keepRdd, JavaPairRDD<Key, Value> deleteRdd) {
    return sc.union(
            keepRdd.map(tuple -> new KV(Dataset.KEEP, tuple._1, tuple._2)), deleteRdd.map(tuple -> new KV(Dataset.DELETE, tuple._1, tuple._2)))
        .keyBy(new KeyWithoutTimestampFun())
        .groupByKey()
        .map(new BuildCheckFun());
  }

  /**
   * Runs the given delete checks in batches and returns an RDD of failed checks.
   *
   * @param checkRdd An RDD of DeleteChecks to run.
   * @return An RDD of failed checks.
   */
  JavaRDD<CheckResult> runChecks(JavaRDD<DeleteCheck> checkRdd) {
    return checkRdd.mapPartitions(new RunCheckFun(runCheckConf));
  }
}
