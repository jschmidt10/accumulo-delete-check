package com.github.jschmidt10.delete.check;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import org.apache.accumulo.core.client.AccumuloException;
import org.apache.accumulo.core.client.AccumuloSecurityException;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Value;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.serializer.KryoSerializer;

public final class Launcher {

  private static final String USERNAME = "USERNAME";
  private static final String PASSwORD = "PASSwORD";
  private static final String INSTANCE = "WAREHOUSE_INSTANCE";
  private static final String ZOOKEEPERS = "WAREHOUSE_ZOOKEEPERS";

  @Parameter(
      names = {"-b", "--base-table"},
      description = "The name of the Accumulo 'base' table",
      required = true)
  private String baseTable;

  @Parameter(
      names = {"-d", "--delete-table"},
      description = "The name of the Accumulo 'delete' table",
      required = true)
  private String deleteTable;

  @Parameter(
      names = {"-i", "--ignored-regex"},
      description = "An optional regex that will defeat matching keys")
  private String ignoredKeyRegex;

  @Parameter(
      names = {"-k", "--keep-table"},
      description = "The name of the Accumulo 'keep' table",
      required = true)
  private String keepTable;

  @Parameter(
      names = {"-o", "--output-file"},
      description = "The file to write the failed results into",
      required = true)
  private String outputFile;

  @Parameter(
      names = {"-p", "--num-partitions"},
      description = "The number of desired partitions for the keep and delete tables in the Spark job.")
  private Integer desiredPartitions;

  @Parameter(
      names = {"-s", "--scan-threads"},
      description = "The number of Accumulo scan threads used to scan the baseTable")
  private int scanThreads = 32;

  @Parameter(
      names = {"-t", "--batch-size"},
      description = "The number of keys to scan from the baseTable at once")
  private int batchSize = 24_000;

  // For JCommander
  public Launcher() {}

  public void run() throws IOException, AccumuloSecurityException, AccumuloException {
    Path outputPath = Args.requireFileWritable(Paths.get(outputFile), "outputFile");

    SparkConf conf = new SparkConf().setAppName("Delete Check of " + baseTable);
    conf.set("spark.serializer", KryoSerializer.class.getName());
    conf.registerKryoClasses(new Class<?>[] {Key.class, Value.class, KV.class, DeleteCheck.class, CheckResult.class});

    try (JavaSparkContext sc = new JavaSparkContext(conf)) {
      AccumuloProperties accumuloProps = readAccumuloPropsFromEnv();

      BuildCheckConf buildCheckConf = new BuildCheckConf.Builder().ignoredKeyRegex(ignoredKeyRegex).desiredPartitions(desiredPartitions).build();

      RunCheckConf runCheckConf =
          new RunCheckConf.Builder()
              .baseTable(baseTable)
              .batchSize(batchSize)
              .scanThreads(scanThreads)
              .connectorFactory(new AccumuloConnectorFactory(accumuloProps))
              .build();

      DeleteCheckJob job = new DeleteCheckJob(sc, accumuloProps, buildCheckConf, runCheckConf);
      Collection<CheckResult> failedResults = job.run(keepTable, deleteTable);
      writeResults(outputPath, failedResults);
    }
  }

  private AccumuloProperties readAccumuloPropsFromEnv() {
    String username = requireEnvVar(USERNAME);
    String password = requireEnvVar(PASSwORD);
    String instanceName = requireEnvVar(INSTANCE);
    String zookeepers = requireEnvVar(ZOOKEEPERS);

    return new AccumuloProperties(username, password, instanceName, zookeepers);
  }

  private String requireEnvVar(String varName) {
    return Args.requireNotBlank(System.getenv(varName), "env var " + varName);
  }

  private void writeResults(Path outputPath, Collection<CheckResult> failedResults) throws IOException {
    try (PrintWriter writer = new PrintWriter(Files.newBufferedWriter(outputPath))) {
      failedResults.forEach(writer::println);
    }
  }

  public static void main(String[] args) throws IOException, AccumuloSecurityException, AccumuloException {
    Launcher launcher = new Launcher();
    JCommander jCommander = new JCommander(launcher);

    try {
      jCommander.parse(args);
    } catch (ParameterException e) {
      jCommander.usage();
      System.exit(1);
    }

    launcher.run();
  }
}
