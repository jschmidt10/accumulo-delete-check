package com.github.jschmidt10.delete.check;

import java.io.Serializable;
import java.util.Objects;

/** The configurations needed by the "run checks" portion of {@link DeleteCheckJob}. */
public final class RunCheckConf implements Serializable {

  public final String baseTable;
  public final TableStrategy tableStrategy;
  public final ConnectorFactory connectorFactory;
  public final int batchSize;
  public final int scanThreads;

  public RunCheckConf(String baseTable, TableStrategy tableStrategy, ConnectorFactory connectorFactory, int batchSize, int scanThreads) {
    this.baseTable = Objects.requireNonNull(baseTable, "baseTable cannot be null");
    this.tableStrategy = Objects.requireNonNull(tableStrategy, "tableStrategy cannot be null");
    this.connectorFactory = Objects.requireNonNull(connectorFactory, "connectorFactory cannot be null");
    this.batchSize = requireGreaterThanZero(batchSize, "batchSize");
    this.scanThreads = requireGreaterThanZero(scanThreads, "scanThreads");
  }

  private int requireGreaterThanZero(int num, String name) {
    if (num <= 0) {
      throw new IllegalArgumentException(name + " must be greater than 0");
    }
    return num;
  }

  /** A Builder for {@link RunCheckConf} */
  public static class Builder {
    private String baseTable;
    private TableStrategy tableStrategy = new BasicTableStrategy();
    private ConnectorFactory connectorFactory;
    private int batchSize = 50_000;
    private int scanThreads = 4;

    public Builder baseTable(String baseTable) {
      this.baseTable = baseTable;
      return this;
    }

    public Builder tableStrategy(TableStrategy tableStrategy) {
      this.tableStrategy = tableStrategy;
      return this;
    }

    public Builder connectorFactory(ConnectorFactory connectorFactory) {
      this.connectorFactory = connectorFactory;
      return this;
    }

    public Builder batchSize(int batchSize) {
      this.batchSize = batchSize;
      return this;
    }

    public Builder scanThreads(int scanThreads) {
      this.scanThreads = scanThreads;
      return this;
    }

    public RunCheckConf build() {
      return new RunCheckConf(baseTable, tableStrategy, connectorFactory, batchSize, scanThreads);
    }
  }
}
