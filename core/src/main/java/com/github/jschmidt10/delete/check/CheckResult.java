package com.github.jschmidt10.delete.check;

import java.util.Objects;
import org.apache.accumulo.core.data.Key;

/**
 * An Accumulo key that failed the delete check.
 *
 * <p>Contains both the failing key and an associated error type.
 */
public final class CheckResult {

  public final boolean success;
  public final Key key;
  public final Error error;

  public CheckResult(boolean success, Key key, Error error) {
    this.success = success;
    this.key = Objects.requireNonNull(key);
    this.error = Objects.requireNonNull(error);
  }

  /** Error types for failed checks. */
  public enum Error {
    SHOULD_HAVE_BEEN_KEPT,
    SHOULD_HAVE_BEEN_DELETED
  }

  /**
   * Factory method for creating a successful check.
   *
   * @param key The key that was properly kept/deleted.
   * @return A successful CheckResult
   */
  public static CheckResult success(Key key) {
    return new CheckResult(true, key, null);
  }

  /**
   * Factory method for creating a failure for a key that should have been kept but was not.
   *
   * @param key The missing key
   * @return A failed CheckResult
   */
  public static CheckResult shouldHaveBeenKept(Key key) {
    return new CheckResult(false, key, Error.SHOULD_HAVE_BEEN_KEPT);
  }

  /**
   * Factory method for creating a failure for a key that should have been deleted but was not.
   *
   * @param key The present key
   * @return A failed CheckResult
   */
  public static CheckResult shouldHaveBeenDeleted(Key key) {
    return new CheckResult(false, key, Error.SHOULD_HAVE_BEEN_DELETED);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    CheckResult that = (CheckResult) o;
    return key.equals(that.key) && error == that.error;
  }

  @Override
  public int hashCode() {
    return Objects.hash(key, error);
  }

  @Override
  public String toString() {
    return "CheckResult{" + "success=" + success + ", key=" + key + ", error=" + error + '}';
  }
}
