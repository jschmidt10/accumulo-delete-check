package com.github.jschmidt10.delete.check;

import java.io.Serializable;
import org.apache.accumulo.core.client.AccumuloException;
import org.apache.accumulo.core.client.AccumuloSecurityException;
import org.apache.accumulo.core.client.Connector;
import org.apache.accumulo.core.security.Authorizations;

/** Creates new {@link Connector}s. */
public interface ConnectorFactory extends Serializable {

  /**
   * Creates a new Connector
   *
   * @return a connector
   */
  Connector newConnector() throws AccumuloException, AccumuloSecurityException;

  /**
   * Gets all user Authorizations
   *
   * @param connectorFactory A ConnectorFactory
   * @return All the user authorizations
   * @throws AccumuloSecurityException If the user credentials are incorrect
   * @throws AccumuloException If any other Accumulo errors occur
   */
  static Authorizations getAllAuths(ConnectorFactory connectorFactory) throws AccumuloSecurityException, AccumuloException {
    return getAllAuths(connectorFactory.newConnector());
  }

  /**
   * Gets all user Authorizations
   *
   * @param connector A Connector
   * @return All the user authorizations
   * @throws AccumuloSecurityException If the user credentials are incorrect
   * @throws AccumuloException If any other Accumulo errors occur
   */
  static Authorizations getAllAuths(Connector connector) throws AccumuloSecurityException, AccumuloException {
    return connector.securityOperations().getUserAuthorizations(connector.whoami());
  }
}
